<?php
declare(strict_types=1);

/*
 * This file is part of the data-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\DataObject\Field;

/**
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
abstract class AbstractField implements FieldInterface
{

    /**
     * Is mandatory?
     *
     * @var bool
     */
    private $mandatory = false;

    /**
     * Field name
     *
     * @var string
     */
    private $name = '';

    /**
     * Is read only?
     *
     * @var bool
     */
    private $readOnly = false;

    /**
     * Is value set via setValue() ?
     *
     * @var bool
     */
    private $valueSet = false;

    /**
     * Check mandatory, but value not set
     *
     * @throws \LogicException If mandatory and value not set
     */
    final public function checkMandatory()
    {
        if ($this->isMandatory() && !$this->isValueSet()) {
            throw new \LogicException(\sprintf('Field "%s" is mandatory, but value not set', $this->getName()));
        }
    }

    /**
     * Check read only flag set
     *
     * @throws \LogicException If read only flag set
     */
    final public function checkReadOnly()
    {
        if ($this->isReadOnly()) {
            throw new \LogicException(\sprintf('Field "%s" is read only', $this->getName()));
        }
    }

    /**
     * Get field name
     *
     * @return string
     */
    final public function getName(): string
    {
        return $this->name;
    }

    /**
     * Is mandatory
     *
     * @return bool
     */
    final public function isMandatory(): bool
    {
        return $this->mandatory;
    }

    /**
     * Is read only?
     *
     * @return bool
     */
    final public function isReadOnly(): bool
    {
        return $this->readOnly;
    }

    /**
     * Is value set via setValue() ?
     *
     * @return bool
     */
    final public function isValueSet(): bool
    {
        return $this->valueSet;
    }

    /**
     * On value set
     *
     * @return $this
     */
    final protected function onValueSet()
    {
        $this->valueSet = true;

        return $this;
    }

    /**
     * Set mandatory flag
     *
     * @param bool $flag
     * @return $this
     */
    final protected function setMandatory(bool $flag)
    {
        $this->mandatory = $flag;

        return $this;
    }

    /**
     * Set field name
     *
     * @param string $name
     * @return $this
     * @throws \InvalidArgumentException If $name is empty
     * @throws \LogicException If name already set
     */
    final protected function setName(string $name)
    {
        if ('' == $name) {
            throw new \InvalidArgumentException('$name must not be empty');
        } elseif ('' != $this->name) {
            throw new \LogicException('Name already set');
        }

        $this->name = $name;

        return $this;
    }

    /**
     * Set read only flag
     *
     * @param bool $flag
     * @return $this
     */
    final protected function setReadOnly(bool $flag)
    {
        $this->readOnly = $flag;

        return $this;
    }
}

<?php
declare(strict_types=1);

/*
 * This file is part of the data-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\DataObject\Field;

/**
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
interface FieldInterface
{

    /**
     * Get field name
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Get field value
     *
     * @return mixed
     */
    public function getValue();

    /**
     * Is mandatory
     *
     * @return bool
     */
    public function isMandatory(): bool;

    /**
     * Is read only?
     *
     * @return bool
     */
    public function isReadOnly(): bool;

    /**
     * Is value set via setValue() ?
     *
     * @return bool
     */
    public function isValueSet(): bool;
}

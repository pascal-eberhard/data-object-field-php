<?php
declare(strict_types=1);

/*
 * This file is part of the data-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\DataObject\Field;

/**
 * For bool value
 *
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
interface BoolFieldInterface extends FieldInterface
{

    /**
     * Get field value
     *
     * @return bool
     */
    public function getValue(): bool;

    /**
     * Set field value
     *
     * @param bool $value
     * @return $this
     * @throws \LogicException If read only flag set
     * @see FieldInterface::isReadOnly()
     */
    public function setValue(bool $value);
}

<?php
declare(strict_types=1);

/*
 * This file is part of the data-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\DataObject\Field;

/**
 * For int value
 *
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
interface IntFieldInterface extends FieldInterface
{

    /**
     * Get field value
     *
     * @return int
     */
    public function getValue(): int;

    /**
     * Set field value
     *
     * @param int $value
     * @return $this
     * @throws \LogicException If read only flag set
     * @see FieldInterface::isReadOnly()
     */
    public function setValue(int $value);
}

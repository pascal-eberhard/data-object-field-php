<?php
declare(strict_types=1);

/*
 * This file is part of the data-object-php package.
 *
 * (c) Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 *
 * For the full copyright and license information, please view the LICENSE
 */

namespace PEPrograms\DataObject\Field;

/**
 * For int value
 *
 * @copyright 2019 Pascal Eberhard <pascal-eberhard-programming@posteo.de>
 */
final class IntField extends AbstractField implements IntFieldInterface
{

    /**
     * Field value
     *
     * @var int
     */
    private $value = 0;

    /**
     * Get field value
     *
     * @return int
     */
    public function getValue(): int
    {
        $this->checkMandatory();

        return $this->value;
    }

    /**
     * Set field value
     *
     * @param int $value
     * @return $this
     * @throws \LogicException If read only flag set
     * @see FieldInterface::isReadOnly()
     */
    public function setValue(int $value)
    {
        $this->checkReadOnly();
        $this->value = $value;
        $this->onValueSet();

        return $this;
    }
}
